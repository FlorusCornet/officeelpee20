#include <msp430.h> 
#include <stdint.h>
#include <string.h>
#include "lib/gpio.h"
#include "lib/i2c.h"
#include "lib/oled.h"
#include "lib/fonts.h"
#include "lib/temperatuur.h"
#include <stdio.h>

/*
 * Dit is de testcode voor de UNITTEST van de Display voor groepje 3
 * P1.0 start knop
 * P1.1 stop knop
 * P1.2 reset knop
 * P1.6 op SCL van display
 * P1.7 op SDA van display
 */

int seconden = 0;
int centimeter = 0;
int RGB = 0;

#pragma vector = TIMER0_A1_VECTOR
__interrupt void tijd(void)
{
    static int teller = 0;
    static int teller2 = 0;
    teller++;
    centimeter++;
    teller2++;

    if (teller >= 100)
    {
        seconden++;
        teller = 0;
    }
    if (teller2 >= 500)
    {
        RGB++;
        teller2 = 0;
    }
    TA0CTL &= ~(TAIFG); // flags resetten
}


#pragma vector = PORT1_VECTOR
__interrupt void reset(void)
{
    if ((P1IFG & BIT0) != 0) // START KNOP
    {
        oledClearScreen();
        oledPrint(2,4, "START...", big);
        TA0CTL |= (TAIE); // counter starten
    }
    else if ((P1IFG & BIT1) != 0) // STOP KNOP
    {
        TA0R = 19999; // zodat je de knop kan indrukken tijdens de timer interrupt
        TA0CTL &= ~(TAIE); // counter stoppen
        oledClearScreen();
        char tekst[15]; // seconden + %d + 1
        char tekst1[17]; // afstand + %d +1
        char tekst2[12]; // R/G/B: +%d +1
        sprintf(tekst, "seconden: %d", seconden);
        sprintf(tekst1, "afstand: %d cm", centimeter);
        sprintf(tekst2, "R/G/B: %d", RGB);
        oledPrint(1, 1, tekst1, small);
        oledPrint(1, 4, tekst2, small);
        oledPrint(1, 7, tekst, small);
    }
    else if ((P1IFG & BIT2) != 0) // RESET KNOP
    {
        oledClearScreen();
        oledPrint(2,4, "RESET . . .", big);
        seconden = 0;
        centimeter = 0;
        RGB = 0;
    }
    P1IFG &= ~(BIT0 | BIT1 | BIT2);
}



void main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // Stop watchdog timer

    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ; // Set range
    DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation

    /* Het display heeft na het aansluiten van de voedingsspanning even
     * de tijd nodig om op te starten voordat hij aangestuurd kan worden.
     * We wachten hier 20ms.
     */
    __delay_cycles(320000);

    // Zet display aan
    oledInitialize();
    // Eventueel flippen
    oledSetOrientation(FLIPPED);
    // Begin met een leeg scherm
    oledClearScreen();


    P1DIR &= ~(BIT0 | BIT1 | BIT2); // input pinnetje
    P1REN |= (BIT0 | BIT1 | BIT2); // weerstand aanzetten
    P1OUT &= ~(BIT0 | BIT1 | BIT2); // pull down
    P1IE |= (BIT0 | BIT1 | BIT2); // interrupt enable
    P1IES |= (BIT0 | BIT1 | BIT2); // opgaande flank
    P1IFG = ~(BIT0 | BIT1 | BIT2); // flags resetten

    TA0CTL = 0;
    TA0CTL |= (TASSEL_2 | ID_3 | MC_1); // smclk, divider van 8, up mode voor TA0CCR0, timer interrupt enable
    TA0CCR0 = 19999; // is 10 ms want 16mhz/8 = 2mhz per sec 2.000.000/100 = 20000 dat -1 is 19999

    __enable_interrupt();

    while (1)
    {

    }
    return 0;
}
